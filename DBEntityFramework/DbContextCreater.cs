﻿using Microsoft.EntityFrameworkCore;

namespace DBEntityFramework
{
    class DbContextCreater : DbContext
    {
        // Field
        public Professor Professor { get; set; }
        public Student Student { get; set; }
        public Subject Subject { get; set; }
        public Certificate Certificate { get; set; }
        public ProfessorCertificate ProfessorCertificate { get; set; }

        // Instantiate tables
        public DbSet<Professor> Professors { get; set; }
        public DbSet<Student> Students { get; set; }
        public DbSet<Subject> Subjects { get; set; }
        public DbSet<Certificate> Certificates { get; set; }
        public DbSet<ProfessorCertificate> ProfessorCertificates { get; set; }


        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Data Source = PC7265\\SQLEXPRESS; Initial Catalog = PostGradFirstDB; Integrated Security = True;");
        }

        protected override  void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ProfessorCertificate>().HasKey(pq => new { pq.ProfessorID, pq.CertificateID });
        }
    }
}
