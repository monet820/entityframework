﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBEntityFramework
{
    class Professor
    {
        // PK
        public int Id { get; set; }
        // Field
        public string FirstName { get; set; }
        public string LastName { get; set; }

        // Foreign key. Make one professor be able to have one subject
        public int? SubjectID { get; set; }
        public Subject Subject { get; set; }

        // Foreign key. Make one professor have many certificates, through the class ProfessorCertificate.
        public ICollection<ProfessorCertificate> ProfessorCertificates { get; set; }

    }
}
