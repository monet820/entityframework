﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBEntityFramework
{
    // Class to enable many to many relationship between Professor and Certificate.
    class ProfessorCertificate
    {
        // Forein key to Professor.
        public int ProfessorID { get; set; }
        public Professor Professor { get; set; }
        
        // Forein key to Certificate.
        public int CertificateID { get; set; }
        public Certificate Certificate { get; set; }
    }
}
