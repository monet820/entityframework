﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBEntityFramework
{
    class Certificate
    {
        // Primary key
        public int Id { get; set; }

        // Certification values
        public string Name{ get; set; }
        public string Field { get; set; }
        public DateTime ExpirationDate{ get; set; }

        // Foreign key to add many Professor to one Cerificate
        public ICollection<ProfessorCertificate> ProfessorsCertificates { get; set; }
    }
}
