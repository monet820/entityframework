﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DBEntityFramework.Migrations
{
    public partial class ReformatFK_2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Students_Professors_ProfessorID",
                table: "Students");

            migrationBuilder.RenameColumn(
                name: "ProfessorID",
                table: "Students",
                newName: "ProfessorId");

            migrationBuilder.RenameIndex(
                name: "IX_Students_ProfessorID",
                table: "Students",
                newName: "IX_Students_ProfessorId");

            migrationBuilder.AlterColumn<int>(
                name: "ProfessorId",
                table: "Students",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AddColumn<int>(
                name: "SubjectID",
                table: "Professors",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Professors_SubjectID",
                table: "Professors",
                column: "SubjectID",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Professors_Subjects_SubjectID",
                table: "Professors",
                column: "SubjectID",
                principalTable: "Subjects",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Students_Professors_ProfessorId",
                table: "Students",
                column: "ProfessorId",
                principalTable: "Professors",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Professors_Subjects_SubjectID",
                table: "Professors");

            migrationBuilder.DropForeignKey(
                name: "FK_Students_Professors_ProfessorId",
                table: "Students");

            migrationBuilder.DropIndex(
                name: "IX_Professors_SubjectID",
                table: "Professors");

            migrationBuilder.DropColumn(
                name: "SubjectID",
                table: "Professors");

            migrationBuilder.RenameColumn(
                name: "ProfessorId",
                table: "Students",
                newName: "ProfessorID");

            migrationBuilder.RenameIndex(
                name: "IX_Students_ProfessorId",
                table: "Students",
                newName: "IX_Students_ProfessorID");

            migrationBuilder.AlterColumn<int>(
                name: "ProfessorID",
                table: "Students",
                type: "int",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Students_Professors_ProfessorID",
                table: "Students",
                column: "ProfessorID",
                principalTable: "Professors",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
