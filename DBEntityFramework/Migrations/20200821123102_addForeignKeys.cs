﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DBEntityFramework.Migrations
{
    public partial class addForeignKeys : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ProfessorID",
                table: "Students",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Students_ProfessorID",
                table: "Students",
                column: "ProfessorID");

            migrationBuilder.AddForeignKey(
                name: "FK_Students_Professors_ProfessorID",
                table: "Students",
                column: "ProfessorID",
                principalTable: "Professors",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Students_Professors_ProfessorID",
                table: "Students");

            migrationBuilder.DropIndex(
                name: "IX_Students_ProfessorID",
                table: "Students");

            migrationBuilder.DropColumn(
                name: "ProfessorID",
                table: "Students");
        }
    }
}
