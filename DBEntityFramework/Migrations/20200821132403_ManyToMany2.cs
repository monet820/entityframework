﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DBEntityFramework.Migrations
{
    public partial class ManyToMany2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ProfessorCertificate_Certificate_CertificateID",
                table: "ProfessorCertificate");

            migrationBuilder.DropForeignKey(
                name: "FK_ProfessorCertificate_Professors_ProfessorID",
                table: "ProfessorCertificate");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ProfessorCertificate",
                table: "ProfessorCertificate");

            migrationBuilder.RenameTable(
                name: "ProfessorCertificate",
                newName: "ProfessorCertificates");

            migrationBuilder.RenameIndex(
                name: "IX_ProfessorCertificate_CertificateID",
                table: "ProfessorCertificates",
                newName: "IX_ProfessorCertificates_CertificateID");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ProfessorCertificates",
                table: "ProfessorCertificates",
                columns: new[] { "ProfessorID", "CertificateID" });

            migrationBuilder.AddForeignKey(
                name: "FK_ProfessorCertificates_Certificate_CertificateID",
                table: "ProfessorCertificates",
                column: "CertificateID",
                principalTable: "Certificate",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ProfessorCertificates_Professors_ProfessorID",
                table: "ProfessorCertificates",
                column: "ProfessorID",
                principalTable: "Professors",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ProfessorCertificates_Certificate_CertificateID",
                table: "ProfessorCertificates");

            migrationBuilder.DropForeignKey(
                name: "FK_ProfessorCertificates_Professors_ProfessorID",
                table: "ProfessorCertificates");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ProfessorCertificates",
                table: "ProfessorCertificates");

            migrationBuilder.RenameTable(
                name: "ProfessorCertificates",
                newName: "ProfessorCertificate");

            migrationBuilder.RenameIndex(
                name: "IX_ProfessorCertificates_CertificateID",
                table: "ProfessorCertificate",
                newName: "IX_ProfessorCertificate_CertificateID");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ProfessorCertificate",
                table: "ProfessorCertificate",
                columns: new[] { "ProfessorID", "CertificateID" });

            migrationBuilder.AddForeignKey(
                name: "FK_ProfessorCertificate_Certificate_CertificateID",
                table: "ProfessorCertificate",
                column: "CertificateID",
                principalTable: "Certificate",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ProfessorCertificate_Professors_ProfessorID",
                table: "ProfessorCertificate",
                column: "ProfessorID",
                principalTable: "Professors",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
