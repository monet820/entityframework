﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DBEntityFramework.Migrations
{
    public partial class newProfessor : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Professors_Subjects_SubjectID",
                table: "Professors");

            migrationBuilder.DropIndex(
                name: "IX_Professors_SubjectID",
                table: "Professors");

            migrationBuilder.AlterColumn<int>(
                name: "SubjectID",
                table: "Professors",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.CreateIndex(
                name: "IX_Professors_SubjectID",
                table: "Professors",
                column: "SubjectID",
                unique: true,
                filter: "[SubjectID] IS NOT NULL");

            migrationBuilder.AddForeignKey(
                name: "FK_Professors_Subjects_SubjectID",
                table: "Professors",
                column: "SubjectID",
                principalTable: "Subjects",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Professors_Subjects_SubjectID",
                table: "Professors");

            migrationBuilder.DropIndex(
                name: "IX_Professors_SubjectID",
                table: "Professors");

            migrationBuilder.AlterColumn<int>(
                name: "SubjectID",
                table: "Professors",
                type: "int",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Professors_SubjectID",
                table: "Professors",
                column: "SubjectID",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Professors_Subjects_SubjectID",
                table: "Professors",
                column: "SubjectID",
                principalTable: "Subjects",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
