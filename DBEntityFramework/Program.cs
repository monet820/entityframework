﻿using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;

namespace DBEntityFramework
{
    class Program
    {
        // Method to add Certificate to Certificate table.
        private static void AddCertificate(Certificate newCertificate)
        {
            try
            {
                using (DbContextCreater dbContextCreater = new DbContextCreater())
                {
                    dbContextCreater.Certificates.Add(newCertificate);
                    dbContextCreater.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        // Method to add Subject to Subject table in database.
        private static void AddSubject(Subject newSubject)
        {
            try
            {
                using (DbContextCreater dbContextCreater = new DbContextCreater())
                {
                    dbContextCreater.Subjects.Add(newSubject);
                    dbContextCreater.SaveChanges();
                }
            }
            // 
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        // Serialize Professor to Json. Prints collection.
        public static void GetProfessorsJson()
        {
            using (DbContextCreater dbContextCreater = new DbContextCreater())
            {
                // Creating List of Professors using the dbcontext tolist method.
                List<Professor> allProfessors = dbContextCreater.Professors.ToList();

                // Serialize a object to Json String.
                string jsonStudent1 = JsonConvert.SerializeObject(allProfessors, Formatting.Indented, new JsonSerializerSettings()
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                });

                // Print to console the Json string.
                Console.WriteLine($"{jsonStudent1}");
            }
        }
        // Get the next input from console as a string.
        public static string GetUserInput()
        {
            string newWordToGet = Console.ReadLine().ToLower();
            return newWordToGet;
        }
        // Deserialize json string.
        public static void DeserializeJson(object JsonObject, string JsonString)
        {
            JsonObject = JsonConvert.DeserializeObject<Object>(JsonString);
        }

        // Add new professorCertificate to the database.
        public static void AddProfessorCertificate(Professor professor, Certificate certificate)
        {
            using (DbContextCreater dbContext = new DbContextCreater())
            {
                // Creating new professorCertificate to hold the professor ID and Certificate ID.
                ProfessorCertificate prof = new ProfessorCertificate();
                prof.CertificateID = certificate.Id;
                prof.ProfessorID = professor.Id;

                //add instances to context
                dbContext.ProfessorCertificates.Add(prof);

                //call SaveChanges from context to confirm inserts
                dbContext.SaveChanges();
            }
        }
        // Removes the connection between a certificate and a professor.
        public static void RemoveProfessorCertificate(Professor professor, Certificate certificate)
        {
            using (DbContextCreater dbContext = new DbContextCreater())
            {
                ProfessorCertificate prof = new ProfessorCertificate();
                prof.CertificateID = certificate.Id;
                prof.ProfessorID = professor.Id;

                // remove instance from context.
                dbContext.ProfessorCertificates.Remove(prof);

                //call SaveChanges from context to confirm delete.
                dbContext.SaveChanges();
            }
        }
        // Method to add Student to Student table.
        private static void AddStudent(Student newStudent)
        {
            try
            {
                using (DbContextCreater dbContextCreater = new DbContextCreater())
                {
                    dbContextCreater.Students.Add(newStudent);
                    dbContextCreater.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.InnerException.Message);
            }
        }

        // Method to add professor to Professor table.
        private static void AddProfessor(Professor newProfessor)
        {
            try
            {
                using (DbContextCreater dbContextCreater = new DbContextCreater())
                {
                    dbContextCreater.Professors.Add(newProfessor);
                    dbContextCreater.SaveChanges();
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        // main function, what the program runs.
        // TODO: Add methods, for cleaner code.
        static void Main(string[] args)
        {
            try
            {
                Console.WriteLine("Type anything to run program, type json to handle json.");
                string chooseStart = GetUserInput();
                if (chooseStart == "json")
                {
                    GetProfessorsJson();
                }
                else
                {
                    // adding bool for when to exit the program.
                    bool running = true;
                    while (running)
                    {
                        Console.WriteLine("type create, read, update or delete to do CRUD to database.");
                        string checkWord = GetUserInput();

                        // Checking if create was the word the user typed.
                        if (checkWord == "create")
                        {
                            Console.WriteLine("What do you want to create, a person, subject, certificate or a professor?");
                            string newCheck = GetUserInput();

                            if (newCheck == "student")
                            {
                                Student newStudent = new Student();
                                Console.WriteLine("Write your gender.");
                                newStudent.Gender = GetUserInput();
                                Console.WriteLine("Write your firstname.");
                                newStudent.FirstName = GetUserInput();
                                Console.WriteLine("Write your lastname.");
                                newStudent.LastName = GetUserInput();

                                AddStudent(newStudent);
                            }
                            // Check if subject is the one the user wanted to create.
                            if (newCheck == "subject")
                            {
                                // Create new subject if, the user wanted to create professor.
                                Subject newStudent = new Subject();
                                Console.WriteLine("Write the subjects name.");
                                newStudent.Name = GetUserInput();

                                AddSubject(newStudent);
                            }
                            // Check if professor is the one the user wanted to create.
                            if (newCheck == "professor")
                            {
                                // Create new professor if, the user wanted to create professor.
                                Professor newProfessor = new Professor();
                                Console.WriteLine("Write your firstname.");
                                newProfessor.FirstName = GetUserInput();
                                Console.WriteLine("Write your lastname.");
                                newProfessor.LastName = GetUserInput();

                                // Using the method, to pass the professor to the database.
                                AddProfessor(newProfessor);
                            }
                            if (newCheck == "certificate")
                            {
                                // Create new professor if, the user wanted to create professor.
                                Certificate newCertificate = new Certificate();
                                Console.WriteLine("Write name of the certificate.");
                                newCertificate.Name = GetUserInput();
                                DateTime expireDate;
                                Console.WriteLine("When does the certificate expire? enter in format MM/DD/YYYY: ");
                                //accepts date in MM/dd/yyyy format
                                expireDate = DateTime.Parse(Console.ReadLine());
                                newCertificate.ExpirationDate = expireDate;

                                // Using the method, to pass the professor to the database.
                                AddCertificate(newCertificate);
                            }
                        }
                        // Checking if read was the word the user typed.
                        if (checkWord == "read")
                        {
                            Console.WriteLine("Type persons, subjects, certificates or professors to show all of that type.");
                            string readAll = Console.ReadLine();
                            // Checking if persons was the word the user typed.
                            if (readAll == "persons")
                            {
                                using (DbContextCreater dbContext = new DbContextCreater())
                                {
                                    foreach (var item in dbContext.Students.Include(p => p.Subject))
                                    {
                                        if (item.SubjectID == null)
                                        {
                                            Console.WriteLine($"{item.FirstName}, no subject");
                                        }
                                        if (item.SubjectID != null)
                                        {
                                            Console.WriteLine($"{item.FirstName}, {item.Subject.Name}");
                                        }
                                    }
                                }
                            }
                            // Checking if professors was the word the user typed.
                            if (readAll == "professors")
                            {
                                // Creating new dbcontext.
                                using (DbContextCreater dbContext = new DbContextCreater())
                                {
                                    // Getting linked tables professors and subjects.
                                    foreach (var item in dbContext.Professors.Include(p => p.Subject))
                                    {
                                        if (item.SubjectID == null)
                                        {
                                            Console.WriteLine($"{item.FirstName}, no subject");
                                        }
                                        if (item.SubjectID != null)
                                        {
                                            Console.WriteLine($"{item.FirstName}, {item.Subject.Name}");
                                        }
                                    }
                                }
                            }
                            // Checking if subjects was the word the user typed.
                            if (readAll == "subjects")
                            {
                                using (DbContextCreater dbContext = new DbContextCreater())
                                {
                                    foreach (var item in dbContext.Subjects)
                                    {
                                        Console.WriteLine(item.Name);
                                    }
                                }
                            }
                            // Checking if certificates was the word the user typed.
                            if (readAll == "certificates")
                            {
                                using (DbContextCreater dbContext = new DbContextCreater())
                                {
                                    foreach (var item in dbContext.Certificates)
                                    {
                                        Console.WriteLine(item.Name);
                                    }
                                }
                            }
                            // Feedback to user, to tell them easily if there were no items in the list.
                            // TODO: refactor to only show when trying to get all from an empty list.
                            Console.WriteLine("All the items were listed.");
                        }
                        // Checking if update was the word the user typed.
                        if (checkWord == "update")
                        {
                            Console.WriteLine("Do you want to update an professor or a student?");
                            string getType = GetUserInput();
                            // TODO: Refactor update to method.
                            try
                            {

                                if (getType == "professor")
                                {
                                    Console.WriteLine("Type the name of the professor you want to change.");
                                    string updateAll = GetUserInput();


                                    using (DbContextCreater dbContext = new DbContextCreater())
                                    {
                                        // Get the student with the firstname that was wanted. Throws exception if two of same name.
                                        // TODO: Handle the exception, that is thrown when there are multiple students with the same name.
                                        // TODO: Refactor to method, so the method can be used for both the professor and student.
                                        // Checking if the professor the user wanted is in the database.
                                        if (dbContext.Professors.SingleOrDefault(s => s.FirstName == updateAll) == null)
                                        {
                                            Console.WriteLine("You have not chosen a valid Professor.");
                                        }
                                        // Checking if the student the user wanted is in the database.
                                        if (dbContext.Professors.SingleOrDefault(s => s.FirstName == updateAll) != null)
                                        {
                                            var updateProfessor = dbContext.Professors.SingleOrDefault(s => s.FirstName == updateAll);
                                            Console.WriteLine($"You have selected {updateProfessor.FirstName} {updateProfessor.LastName}");

                                            // Asking the user to select if they want to change the first or last name of the professor.
                                            Console.WriteLine("You can change the subject, firstname, certificate or lastname, which one do you want to change?");
                                            string newName = GetUserInput();
                                            // Checking if the user want to update the firstname of a student.
                                            if (newName == "firstname")
                                            {
                                                Console.WriteLine("Type the new First name");
                                                updateProfessor.FirstName = Console.ReadLine();
                                                Console.WriteLine($"The new firstname is {updateProfessor.FirstName}");
                                                dbContext.SaveChanges();
                                            }
                                            // Checking if the user want to update the lastname of a student..
                                            if (newName == "lastname")
                                            {
                                                Console.WriteLine("Type the new Last name");
                                                updateProfessor.LastName = Console.ReadLine();
                                                Console.WriteLine($"The new lastname is {updateProfessor.LastName}");
                                                dbContext.SaveChanges();
                                            }
                                            if (newName == "subject")
                                            {
                                                if (updateProfessor.SubjectID == null)
                                                {
                                                    Console.WriteLine("There are no current subject for that professor");
                                                }
                                                if (updateProfessor.SubjectID != null)
                                                {
                                                    Console.WriteLine($"Current subject for the professor is: {updateProfessor.Subject.Name}");
                                                }
                                                Console.WriteLine("Type the subject you wish to add to the professor.");
                                                string updateSubject = GetUserInput();

                                                if (dbContext.Subjects.SingleOrDefault(s => s.Name == updateSubject) == null)
                                                {
                                                    Console.WriteLine("You have not chosen a valid Subject.");
                                                }
                                                // If a subject with the name the user asked for exist, we add it to the professor.
                                                if (dbContext.Subjects.SingleOrDefault(s => s.Name == updateSubject) != null)
                                                {
                                                    var professorSubject = dbContext.Subjects.SingleOrDefault(s => s.Name == updateSubject);
                                                    updateProfessor.SubjectID = professorSubject.Id;
                                                    dbContext.SaveChanges();
                                                }
                                            }
                                            if (newName == "certificate")
                                            {
                                                Console.WriteLine("Do you wish to add an certificate to a professor or remove?");
                                                string updateCertificate = GetUserInput();
                                                if (updateCertificate == "add")
                                                {
                                                    Console.WriteLine("Type the certificate you wish to add to the professor.");
                                                    string updateSubject = GetUserInput();

                                                    if (dbContext.Certificates.SingleOrDefault(s => s.Name == updateSubject) == null)
                                                    {
                                                        Console.WriteLine("You have not chosen a valid Subject.");
                                                    }
                                                    // If a certificate with the name the user asked for exist, we add it to the professor.
                                                    if (dbContext.Certificates.SingleOrDefault(s => s.Name == updateSubject) != null)
                                                    {
                                                        var professorCertificate = dbContext.Certificates.SingleOrDefault(s => s.Name == updateSubject);

                                                        AddProfessorCertificate(updateProfessor, professorCertificate);
                                                    }
                                                }
                                                if (updateCertificate == "remove")
                                                {
                                                    Console.WriteLine("Type the certificate you wish to remove from the professor.");
                                                    string updateSubject = GetUserInput();

                                                    if (dbContext.Certificates.SingleOrDefault(s => s.Name == updateSubject) == null)
                                                    {
                                                        Console.WriteLine("You have not chosen a valid Subject.");
                                                    }
                                                    // If a certificate with the name the user asked for exist, we remove it from the professor.
                                                    if (dbContext.Certificates.SingleOrDefault(s => s.Name == updateSubject) != null)
                                                    {
                                                        var professorCertificate = dbContext.Certificates.SingleOrDefault(s => s.Name == updateSubject);

                                                        RemoveProfessorCertificate(updateProfessor, professorCertificate);
                                                    }
                                                }

                                            }
                                        }
                                    }
                                }
                                if (getType == "student")
                                {

                                    Console.WriteLine("Type the name of the student you want to change.");
                                    string updateAll = GetUserInput();
                                    using (DbContextCreater dbContext = new DbContextCreater())
                                    {
                                        // Get the student with the firstname that was wanted. Throws exception if two of same name.

                                        // Checking if the student the user wanted is in the database.
                                        if (dbContext.Students.SingleOrDefault(s => s.FirstName == updateAll) == null)
                                        {
                                            Console.WriteLine("You have not chosen a valid Person.");
                                        }
                                        // Checking if the student the user wanted is in the database.
                                        if (dbContext.Students.SingleOrDefault(s => s.FirstName == updateAll) != null)
                                        {
                                            var updateStudent = dbContext.Students.SingleOrDefault(s => s.FirstName == updateAll);
                                            Console.WriteLine($"You have selected {updateStudent.FirstName} {updateStudent.LastName}");
                                            Console.WriteLine("type firstname, subject or lastname to change name.");
                                            string newName = GetUserInput();
                                            // Checking if the user want to update the firstname of a student.
                                            if (newName == "firstname")
                                            {
                                                Console.WriteLine("Type the new First name");
                                                updateStudent.FirstName = Console.ReadLine();
                                                Console.WriteLine($"The new first name is {updateStudent.FirstName}");
                                                dbContext.SaveChanges();
                                            }
                                            // Checking if the user want to update the lastname of a student..
                                            if (newName == "lastname")
                                            {
                                                Console.WriteLine("Type the new Last name");
                                                updateStudent.LastName = Console.ReadLine();
                                                Console.WriteLine($"The new last name is {updateStudent.LastName}");
                                                dbContext.SaveChanges();
                                            }
                                            if (newName == "subject")
                                            {
                                                if (updateStudent.SubjectID == null)
                                                {
                                                    Console.WriteLine("There are no current subject for that student.");
                                                }
                                                if (updateStudent.SubjectID != null)
                                                {
                                                    Console.WriteLine($"Current subject for the student is: {updateStudent.Subject.Name}");
                                                }

                                                Console.WriteLine("Type the subject you wish to add to the student.");
                                                string updateSubject = GetUserInput();

                                                if (dbContext.Subjects.SingleOrDefault(s => s.Name == updateSubject) == null)
                                                {
                                                    Console.WriteLine("You have not chosen a valid Subject.");
                                                }
                                                // If a subject with the name the user asked for exist, we add it to the student.
                                                if (dbContext.Subjects.SingleOrDefault(s => s.Name == updateSubject) != null)
                                                {
                                                    var studentSubjectNew = dbContext.Subjects.SingleOrDefault(s => s.Name == updateSubject);
                                                    updateStudent.SubjectID = studentSubjectNew.Id;
                                                    dbContext.SaveChanges();
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                            catch (Exception ex)
                            {
                                Console.WriteLine(ex.Message);
                            }
                        }

                        // Checking if delete was the word the user typed.
                        if (checkWord == "delete")
                        {
                            // Creating a new dbContext to be able to interact with the database.
                            using (DbContextCreater dbContext = new DbContextCreater())
                            {
                                Console.WriteLine("Type the firstname of the student you want to remove.");
                                string deleteFirstName = GetUserInput();
                                Console.WriteLine("Type the lastname of the student you want to remove.");
                                string deleteLastName = GetUserInput();

                                var updateStudent = dbContext.Students.SingleOrDefault(s => s.FirstName == deleteFirstName && s.LastName == deleteLastName);
                                // removing the desired student.
                                dbContext.Students.Remove(updateStudent);
                                // calling the savechanges() to save changes to database.
                                dbContext.SaveChanges();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine("From the catch() in method Main");
            }
        }
    }
}

