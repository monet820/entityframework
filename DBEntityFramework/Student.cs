﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBEntityFramework
{
    class Student
    {
        // Primary key
        public int Id { get; set; }

        // Student values.
        public string Gender { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        // Foreign keys

        // Adding relationship one subject to many student.
        public int? SubjectID { get; set; }
        public Subject Subject { get; set; }

    }
}
