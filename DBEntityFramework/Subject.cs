﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBEntityFramework
{
    class Subject
    {
        // Private key
        public int Id { get; set; }

        // Properties belonging to a Subject
        public string Name { get; set; }


        // Foreign keys
        // Adding relationship one subject to many students.
        public ICollection<Student> Students { get; set; }

        // Foreign key relationship. Add one Subject to one Professor.
        public Professor Professor { get; set; }
    }
}
